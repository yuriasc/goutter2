<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url_id', 'produto', 'preco', 'mbl', 'vendedor'
    ];
}
