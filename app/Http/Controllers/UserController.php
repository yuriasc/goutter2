<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\User;

class UserController extends BaseController
{
    public function index() {
        $users = User::all();
        return response()->json($users);
    }
    
    public function store(Request $request) {
        
    }
}
