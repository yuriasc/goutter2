<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'ip', 'longitude', 'latitude', 'city', 'region_code', 'region_name', 'country_code', 'country_name'
    ];
}
